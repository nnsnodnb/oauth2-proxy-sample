from pathlib import Path

bind = "unix://tmp/gunicorn.sock"
pidfile = str(Path(__file__).parent / "tmp" / "gunicorn.pid")
reload = True
