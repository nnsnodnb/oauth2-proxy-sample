from typing import Any, Dict, Optional

import uvicorn
from fastapi import FastAPI

app = FastAPI(redoc_url=None)


@app.get("/")
async def read_root() -> Dict[str, str]:
    return {"Hello": "World"}


@app.get("/items/{item_id}")
async def read_item(item_id: int, q: Optional[str] = None) -> Dict[str, Any]:
    return {"item_id": item_id, "q": q}


if __name__ == "__main__":
    uvicorn.run("app:app", port=8000, reload=True)
